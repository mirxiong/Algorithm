/*
author: 熊谦智

狱吏问题 -- 等同开关问题 
*/
#include<iostream>
using namespace std;
const int N = 1024;
int a[N];
int main() {
  int n, ans = 0;
  cin >> n;
  for (int i = 1; i * i <= n; i++) {
    ans += 1;
    cout << i*i << " ";
  }
  cout << endl; 
  cout << "最终有" << ans << "个人出去了" << endl;
} 



/*
author: 熊谦智
nyoj77 开灯问题 
*/
//#include<iostream>
//#include<cstring>
//using namespace std;
//const int N = 1024;
//int book[N];
//int main() {
//  int n, k;
//  memset(book,0,sizeof(book));
//  cin >> n >> k;
//  for (int i = 1; i*i <= k; i++) {
//    cout << i*i << " ";
//  }
//  for (int i = k+1; i <= n; i++) {
//    int num = 0;
//    for (int j = 1; j <= k; j++) {
//      if (i%j == 0) {
//        num++;
//      }
//    }
//    if (num%2) {
//      cout << i << " ";
//    }
//  }
//}
