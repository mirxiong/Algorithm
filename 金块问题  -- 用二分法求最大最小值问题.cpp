/*
author: 熊谦智
金块问题  -- 用二分法求数组最大最小值问题 
*/
#include<iostream>
using namespace std;
const int N = 1024;
int a[N];
int searchMax(int left,int right) ; 
int main() {
  int n;
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  } 
  int Max = searchMax(0,n-1);
  cout << Max << endl;
} 
int searchMax(int left,int right) {
  if (left == right) {
    return a[left] > 0 ? a[left] : 0; 
  } else if (left+1 == right) {
    return a[left]>a[right]?a[left]:a[right];
  } else {
    int leftMax = searchMax(left,left+(right-left)/2);
    int rightMax = searchMax(left+(right-left)/2+1,right);
    return leftMax>rightMax?leftMax:rightMax; 
  }
}
