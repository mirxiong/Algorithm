/*
author: 熊谦智
【循环于递归】例题八 
题：任意给十进制数，请从高位达到低位逐位输出个个数字 
*/
#include<iostream>
#include<ctime>
#include<algorithm>
using namespace std;
void f(int n) ;
void circulation(int n) ;
int main() {
  int n;
  cin >> n;
  clock_t start1, start2, end1, end2;
//  time_t start1, start2, end1, end2;
  start1 = clock();
  f(n);
  cout << endl;
  end1 = clock();
  start2 = clock();
  circulation(n);
  end2 = clock();
  cout << start1 << " " << end1 << endl;
  cout << difftime(end1,start1) <<endl;
  cout << "f(n)运行时间为：" << end1-start1 << endl; 
  cout << "circulation(n)运行时间为：" << end2-start2 << endl; 
} 
void circulation(int n) { //循环算法 
  int a[1001], k; 
  while (n) {
    if (n >= 10) {
      a[k++] = n%10;
//      cout << n%10;
    } else {
      a[k++] = n;
//      cout << n << endl;
    }
    n /= 10;
  }
  for (int i = k-1; i >= 0; i--) {
    cout << a[i] << " ";
  }
  cout << endl;
}
void f(int n) {  //递归算法 
  if (n >= 10) {
    f(n/10);
    cout << n%10 << " ";
  } else {
    cout << n << " ";
  }
}
