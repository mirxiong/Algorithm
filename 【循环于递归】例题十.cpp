/*
author: 熊谦智
【循环于递归】例题十 
题：找出n个自然数中（1-n），取r个数的组合 
*/
#include<iostream>
#include<cstring>
using namespace std;
const int N = 1005;
int book[N], a[N];
int n, r, ans;
void dfs(int k) ;
void dfs(int n,int k) ;
int main() {
  cin >> n >> r;
  memset(book,0,sizeof(book));
  ans = 0;
  dfs(0);
  cout << ans << endl;
  ans = 0;
  dfs(n,r);
  cout << ans << endl;
} 
void dfs(int n,int k) { //不会有重复 
  for (int i = n; i >= k; i--) {
    a[k] = i;
    if (k > 1) {
      dfs(i-1,k-1);
    } else {
      for (int j = 1; j <= r; j++) {
        cout << a[j] << " ";
      } 
      ans ++;
      cout << endl;
    }
  }
}
void dfs(int k) { //会有重复 
  if (k == r) {
    for (int i = 0; i < r; i++) {
      cout << a[i] << " ";
    }
    cout << endl;
    ans ++;
    return;
  }
  for (int i = 1; i <= n; i++) {
    if (book[i] == 0) {
      book[i] = 1;
      a[k] = i;
      dfs(k+1);
      book[i] = 0;
    }
  }
}

