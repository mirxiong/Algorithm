/*
author: 熊谦智
残缺棋盘问题  -- 二分法不相识情况 
*/
#include<iostream>
#include<iomanip> 
using namespace std;
const int N = 256;
int Map[N][N];
void cover(int row, int col, int posX,int posY, int n) ;
int main() {
  int n;
  while (cin >> n) {
    int posX, posY;
    cin >> posX >> posY;
    Map[posX][posY] = -1;
    cover(0,0,posX,posY,n);
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        cout << setw(3) << Map[i][j] << " ";
      }
      cout << endl;
    }
  } 
}
void cover(int row, int col, int posX,int posY, int n) {
  if (n < 2) return;
  int mid = n/2;
  if (posX < mid+row && posY < mid+col) {
    cover(row,col,posX,posY,mid);
    Map[row+mid-1][col+mid] = 1;
    Map[row+mid][col+mid] = 1;
    Map[row+mid][col+mid-1] = 1;
    cover(row,col+mid,row+mid-1,col+mid,mid);
    cover(row+mid,col,row+mid,col+mid-1,mid);
    cover(row+mid,col+mid,row+mid,col+mid,mid);
  } else if (posX < mid+row && posY >= mid+col) {  //第二象限 
    cover(row,col+mid,posX,posY,mid);
    Map[row+mid-1][col+mid-1] = 2;
    Map[row+mid][col+mid-1] = 2;
    Map[row+mid][col+mid] = 2;
    cover(row,col,row+mid-1,col+mid-1,mid);
    cover(row+mid,col,row+mid,col+mid-1,mid);
    cover(row+mid,col+mid,row+mid,col+mid,mid);
  } else if (posX >= mid+row && posY < mid+col) {  //第三象限 
    cover(row+mid,col,posX,posY,mid);
    Map[row+mid-1][col+mid-1] = 3;
    Map[row+mid-1][col+mid] = 3;
    Map[row+mid][col+mid] = 3;
    cover(row,col,row+mid-1,col+mid-1,mid);
    cover(row,col+mid,row+mid-1,col+mid,mid);
    cover(row+mid,col+mid,row+mid,col+mid,mid);
  } else {
    cover(row+mid,col+mid,posX,posY,mid);
    Map[row+mid-1][col+mid-1] = 4;
    Map[row+mid-1][col+mid] = 4;
    Map[row+mid][col+mid-1] = 4;
    cover(row,col,row+mid-1,col+mid-1,mid);
    cover(row,col+mid,row+mid-1,col+mid,mid);
    cover(row+mid,col,row+mid,col+mid-1,mid);
  }
}
