/*
author: 熊谦智
【循环于递归】例题七 
题：任意给十进制数，请从低位达到高位逐位输出个个数字 
*/
#include<iostream>
#include<ctime>
#include<algorithm>
using namespace std;
void f(int n) ;
void circulation(int n) ;
int main() {
  int n;
  cin >> n;
  clock_t start1, start2, end1, end2;
  start1 = clock();
  f(n);
  end1 = clock();
  start2 = clock();
  circulation(n);
  end2 = clock();
  cout << start1 << " " << end1 << endl;
  cout << "f(n)运行时间为：" << end1-start1 << endl; 
  cout << "circulation(n)运行时间为：" << end2-start2 << endl; 
} 
void circulation(int n) { //循环算法 
  while (n) {
    if (n >= 10) {
      cout << n%10;
    } else {
      cout << n << endl;
    }
    n /= 10;
  }
}
void f(int n) {  //递归算法 
  if (n >= 10) {
    cout << n%10;
    f(n/10);
  } else {
    cout << n << endl;
  }
}
