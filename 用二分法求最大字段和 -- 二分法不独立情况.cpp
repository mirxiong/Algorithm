/*
author: 熊谦智
用二分法求最大字段和 -- 二分法不独立情况 
*/
#include<iostream>
using namespace std;
#define Max(x,y,z) ((x>y?x:y)>z?(x>y?x:y):z)
const int N = 1024;
int a[N];
int getMaxField(int left,int right) ; 
int main() {
  int n;
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }
  int Max = getMaxField(0,n-1);
  cout << Max << endl;
}
int getMaxField(int left,int right) {
  if (left == right) {
    return a[left] > 0 ? a[left] : 0;
  } else {
    int mid = left + (right-left)/2;
    int leftMax = getMaxField(left,mid);
    int rightMax = getMaxField(mid+1,right);
    int s1 = 0, s2 = 0, leftSum = 0, rightSum = 0;
    for (int i = mid; i >= 0; i--) {
      leftSum += a[i];
      if (leftSum > s1) s1 = leftSum;
    } 
    for (int i = mid+1; i <= right; i++) {
      rightSum += a[i];
      if (rightSum > s2) s2 = rightSum;
    } 
    return Max(leftMax,rightMax,s1+s2);
  }
}
