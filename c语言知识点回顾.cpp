/*
author: 熊谦智

c语言知识点回顾 
*/
#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
int main() {
  int a = 7123;
  printf("%d\n",printf("%d\n",printf("%d\n",a)));  //printf函数有返回值  是其值对应字符串的长度注意末尾的'\0' 
//  printf("%d\n",a);
  
  
  //union的用法和原理 
  union
  {
     int i;
     char a[2];
  }*q, u;
  q =&u;
  q->a[0] = 0x39;
  q->a[1] = 0x38;
  printf("%x\n",q->i); 
  

  int n[3],i,j,k;
  for(i=0;i<3;i++) n[i]=0;
  k=2;
  for (i=0;i<k;i++)
  for (j=0;j<k;j++) n[j]=n[i]+1;
  printf("%d %d\n",n[1], n[0]); 
  
  
  int x[4] = {1,3,5,7};
  int *p = x;
  cout << "*p++ = " << *p++ << endl;
  cout << *p << endl;
  cout << x[0] << endl;
  cout << "*++p = " << *++p << endl;
  cout << *p << endl;
}
