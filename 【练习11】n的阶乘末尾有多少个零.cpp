/*
author: 熊谦智
【练习11】n的阶乘末尾有多少个零 
https://blog.csdn.net/qq_36238595/article/details/53339238
https://blog.csdn.net/hongkangwl/article/details/39666991
https://blog.csdn.net/zbuger/article/details/51076539
*/
#include<iostream>
using namespace std;
#define min(x,y) (x>y?y:x) 
int main() {
  int n;
  while (cin >> n) {  //n的阶乘末尾有多少个0   就是看质因数分解后  有多少个2和5   一对2和5  就有一个零 
    int k1 = 0, k2 = 0, temp = n;
    while (temp) {  //N!质因数分解后  有多少个5 
      k1 += temp/5;
      temp /= 5;
    }
    while (n) {   //N!质因数分解后  有多少个2   看网上解题思路  好像5 的个数一定比2少  说只要求5的个数就行了 
      k2 += n/2;
      n /= 2;
    }
    cout << min(k1,k2) << endl;
  }
}
