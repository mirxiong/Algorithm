/*
author: 熊谦智
【循环于递归】例题九 
题：任何一个正整数都可以用2的幂次方表示 
*/
#include<iostream>
#include<ctime>
#include<cstdio>
using namespace std;
void f(int n,int pos) ;
int main() {
  int n;
  cin >> n;
  clock_t start1 = clock();
  printf("%d = ",n);
  f(n,0);
  cout << endl;
  clock_t end1 = clock();
  printf("运行时间：%d\n",end1 - start1);
//  printf("\b ");
}
void f(int n,int pos) {
  if (n == 1) {
    switch(pos) {
      case 0: {
        cout << "2(0)";
        break;
      }
      case 1: {
        cout << "2";
        break;
      }
      case 2: {
        cout << "2(2)";
        break;
      }
      default: {
        cout << "2("; f(pos,0); cout << ")";
        break;
      }
    }
//    cout << "2(" << pos << ")";
  } else {
    f(n/2,pos+1);
    if (n%2 == 1) {
      switch(pos) {
        case 0: {
          cout << "+2(0)";
          break;
        }
        case 1: {
          cout << "+2";
          break;
        }
        case 2: {
          cout << "+2(2)";
          break;
        }
        default: {
          cout << "+2("; f(pos,0); cout << ")";
          break;
        }
      }
//      cout << "+2(" << pos << ")";
    }
  }
  
}
void f1(int n,int pos) {
  if (n == 0) return;
  f(n/2,pos+1); 
  if(n % 2) {
    cout << "2("; f(pos,0); cout << ")+" ;
  }
}
