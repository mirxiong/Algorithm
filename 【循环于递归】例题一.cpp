/*
author: 熊谦智
计算s(n) = s(n-1) + (-1)^(n-1) /(2*n-1)!
*/
#include<iostream>
#include<cstdio>
#include<cmath>
#include<ctime>
using namespace std;
int main() {
  int n;
  cin >> n;
  double start1, end1;
  start1 = clock();
  double ans = 1;
  int down = 1;
  int sign = 1;
  for (int i = 2; i <= n; i++) {
    sign = -sign;
    down = down * (2*i-2) * (2*i-1);
    ans += sign * 1.0 / down;
  }
  end1 = clock();
  printf("运行时间为: %.2f\n",end1 - start1);
  printf("%.2f\n",ans);
}
