/*
author: 熊谦智
大数相乘 
*/
#include<iostream>
#include<algorithm>
#include<string>
#include<cstring>
using namespace std;
const int N = 1024;
int a[N], b[N], ans[2*N]; 
//大数相乘要用一个新数组储存结果  
//不能用原来的数形成的数组不然在计算过程中会改变原来的数组 
int main() {
  string s1, s2;
  cin >> s1 >> s2;
  int k = 0, len1 = s1.length(), len2 = s2.length(), ansLen = -1;
  memset(ans,0,sizeof(ans));
  for (int i = len1-1; i >= 0; i--) {
    a[k++] = s1[i] - '0';
  }
  k = 0;
  for (int i = len2-1; i >= 0; i--) {
    b[k++] = s2[i] - '0';
  }
//  for (int i = 0; i < len1; i++) cout <<  a[i];
//  cout << endl;
//  for (int i = 0; i < len2; i++) cout <<  b[i];
//  cout << endl;
  for (int i = 0; i < len1; i++) {
    int midVir = 0, j;
    for (j = 0; j < len2; j++) {
      ans[i+j] += b[j] * a[i] + midVir;
      midVir = ans[i+j]/10;
      ans[i+j] %= 10; 
    }
    while (midVir) {
      ans[i+j] += midVir%10;
      midVir /= 10;
      j++;
    }
    ansLen = max(ansLen,i+j);
  }
  for (int i = ansLen-1; i >= 0; i--) {
    cout << ans[i];
  }
  cout << endl;
} 

